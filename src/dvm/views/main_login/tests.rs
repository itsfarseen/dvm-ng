use std::cell::RefCell;
use super::gtk;
use super::gtk::prelude::*;
use gtk_test;

rusty_fork_test! {  // makes all the tests run in different subprocess.

    #[test]
    fn opening_closing_works() {
        super::gtk::init().unwrap();
        struct ControllerStub {}
        impl super::Controller for ControllerStub {
            fn try_login(&self, _user: String, _pass: String) -> bool {
                unimplemented!()
            }

            fn go_home(&self) {
                unimplemented!()
            }
        }
        let controller = ControllerStub{};
        let main_login = super::MainLogin::new();
        let main_login_opened = main_login.open(controller);
        let (_main_login_back, _controller_back) = super::MainLoginOpened::close(main_login_opened);
    }

    #[test]
    fn clicking_login_calls_controller() {
        super::gtk::init().unwrap();
        struct ControllerStub {has_called: RefCell<bool>}
        impl super::Controller for ControllerStub {
            fn try_login(&self, _user: String, _pass: String) -> bool {
                self.has_called.replace(true);
                false
            }

            fn go_home(&self) {
                unimplemented!()
            }
        }
        let controller = ControllerStub {has_called: RefCell::new(false)};
        let main_login = super::MainLogin::new();
        let main_login_opened = main_login.open(controller);

        let window = gtk::Window::new(gtk::WindowType::Toplevel);
        let root_widget = main_login_opened.get_root_widget();
        window.add(&root_widget);
        window.show_all();
        window.activate_focus();
        main_login_opened.ui.user_id.set_text("doesnt_matter");
        main_login_opened.ui.password.set_text("doesnt_matter");
        gtk_test::click(&main_login_opened.ui.login);
        gtk_test::wait(100);
        let (_main_login_back, controller_back) = super::MainLoginOpened::close(main_login_opened);
        assert_eq!(* controller_back.has_called.borrow(), true);
    }

    #[test]
    fn successful_login_goes_home() {
        super::gtk::init().unwrap();
        struct ControllerStub {has_called: RefCell<bool>}
        impl super::Controller for ControllerStub {
            fn try_login(&self, _user: String, _pass: String) -> bool {
                true
            }

            fn go_home(&self) {
                self.has_called.replace(true);
            }
        }
        let controller = ControllerStub {has_called: RefCell::new(false)};
        let main_login = super::MainLogin::new();
        let main_login_opened = main_login.open(controller);
        let window = gtk::Window::new(gtk::WindowType::Toplevel);
        let root_widget = main_login_opened.get_root_widget();
        window.add(&root_widget);
        window.show_all();
        window.activate_focus();
        main_login_opened.ui.user_id.set_text("doesnt_matter");
        main_login_opened.ui.password.set_text("doesnt_matter");
        gtk_test::click(&main_login_opened.ui.login);
        gtk_test::wait(100);
        let (_main_login_back, controller_back) = super::MainLoginOpened::close(main_login_opened);
        assert_eq!(* controller_back.has_called.borrow(), true);
    }

} // rusty_fork_test
