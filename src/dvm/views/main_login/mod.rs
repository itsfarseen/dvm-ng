#[cfg(test)] mod tests;
extern crate gtk;
extern crate glib;
extern crate glib_sys as glib_ffi;
use self::gtk::prelude::*;
use std::rc::{Rc, Weak};
use std::cell::RefCell;
use std::mem;
use super::connected_signal_handler::ConnectedSignalHandler;

pub struct MainLogin {
    ui: UI
}

pub struct MainLoginOpened<T: Controller+'static> {
    ui: UI,
    controller: T,
    signal_handlers_to_cleanup: RefCell<Option<SignalHandlers>>
}

pub trait Controller {
    fn try_login(&self, user: String, pass: String) -> bool;
    fn go_home(&self);
}

static UI_FILE: &str = include_str!("main_login.glade");

ui_struct!(
struct UI {
    root: gtk::Box,
    user_id: gtk::Entry,
    password: gtk::Entry,
    login: gtk::Button,
    login_failed: gtk::Label
});


struct SignalHandlers {
    _login_clicked: ConnectedSignalHandler<gtk::Button>
}

impl MainLogin {
    pub fn new() -> Self {
        Self{
            ui: UI::build(gtk::Builder::new_from_string(UI_FILE))
        }
    }

    pub fn open<T:Controller>(self, controller: T) -> Rc<MainLoginOpened<T>> {
        MainLoginOpened::new_rc(self.ui, controller)
    }

}

impl<T: Controller+'static> MainLoginOpened<T> {

    pub fn close(self_rc: Rc<Self>) -> (MainLogin, T) {
        (*self_rc).cleanup_signal_handlers();
        let self_ = Rc::try_unwrap(self_rc).ok().unwrap();
        (MainLogin{ui: self_.ui}, self_.controller)
    }

    fn cleanup_signal_handlers(&self) {
        let signal_handlers = self.signal_handlers_to_cleanup.replace(None).unwrap();
        mem::drop(signal_handlers);
    }

    fn new_rc(ui: UI, controller: T) -> Rc<Self> {
        let self_rc = Rc::new(Self {
            ui,
            controller,
            signal_handlers_to_cleanup: RefCell::new(None)
        });
        Self::connect_button_callbacks(self_rc.clone());
        self_rc
    }

    fn connect_button_callbacks(self_rc: Rc<Self>) {
        let login_clicked_id = {
            let rc = self_rc.clone();
            (*self_rc).ui.login.connect_clicked(move |_| {
                let user_id = (*rc).get_entered_user_id();
                let password = (*rc).get_entered_password();
                if (*rc).controller.try_login(user_id, password) {
                    (*rc).controller.go_home();
                } else {
                    Self::show_login_failed(rc.clone());
                }
            })
        };

        (*self_rc).signal_handlers_to_cleanup.replace(Some(SignalHandlers {
            _login_clicked: ConnectedSignalHandler::new((*self_rc).ui.login.clone(), login_clicked_id)
        }));

    }

    fn get_entered_user_id(&self) -> String {
        self.ui.user_id.get_text().unwrap_or(String::from(""))
    }

    fn get_entered_password(&self) -> String {
        self.ui.user_id.get_text().unwrap_or(String::from(""))
    }

    fn show_login_failed(self_rc: Rc<Self>) {
        (*self_rc).ui.login_failed.show();
        unsafe{
            glib_ffi::g_timeout_add_seconds(5,
                                            Some(Self::hide_login_failed_cb),
                                            Box::into_raw(Box::new(Rc::downgrade(&self_rc))) as *mut _
                                            // ^^ Move a weak Rc to heap for passing
                                            //    to extern "C" fn callback
            );
            // We are passing a weak reference because:
            // Sometimes, the MainLoginViewOpened may be closed
            // before the timed callback is called.
            // If it were a strong reference, unwrapping self_rc inside main_login_view_opened.close()
            // will panic.
            // By making it a weak reference, we are allowing the possibility of letting the
            // MainLoginViewOpened to close before the callback is executed.
        }
    }

    unsafe extern "C" fn hide_login_failed_cb(data: glib_ffi::gpointer) -> i32{
        let self_weak_ptr: *mut Weak<Self> = data as *mut _;
        let self_weak = Box::from_raw(self_weak_ptr);
        // self_rc will be None, if MainLoginViewOpened is closed before this callback is called.
        if let Some(self_rc) = self_weak.upgrade() {
            (*self_rc).ui.login_failed.hide();
        }
        0 // stop the timer created by g_time_out_add_seconds(..);
    }


    pub fn get_root_widget(&self) -> gtk::Widget {
        self.ui.root.clone().upcast()
    }
}
